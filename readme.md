### GIT
Dans chaque repository GIT y'a deux branches principles:
- master : prod
- develop : pprod/test
- autres : nom-de-feature/votre-nom (ce sont les branches de dev qu'on supprime à chaque fois qu'on temine une feature ou pas si on veut garder la meme branch tout au long de notre dev sur le projet)

à chaque fois qu'on termine une feature, on push notre dev sur notre branch puis on merge sur la develop pour que les autres rebase à leurs tour et récuperent notre travail.

### Linux
- On peut changer les droits pour un user soit d'une maniere octal cad en utilisant les chiffres ou bien ajouter/retirer des permissions à une/plusierus catégorie(s) d'user (r, w, x) et en utilisant la commande chmod

EX : - chmod 777 fichier.txt // donner tous les droits (rwx) 
     - chmod u+rxw fichier.txt // donner tous les droits (rwx) 

- Le proprietaire du fichier ou du dossier peut donner des droits à un user qui n’est ni le propriétaire ni dans son groupe en utilisant la commande chown pour changer le proprietaire, ou ajouter le user dans son groupe ensuite lui attribuer des droits avec la commande chmod



